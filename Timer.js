import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import TimerCountdown from 'react-native-timer-countdown';


export default class Timer extends React.Component {


    constructor () {
        super();
        this.state = {
        }

    }

    render() {
        return (
          <View style={styles.container}>
    	    <TimerCountdown
	            initialSecondsRemaining={1000*60}
	            onTick={secondsRemaining => console.log('tick', secondsRemaining)}
	            onTimeElapsed={() => console.log('complete')}
	            allowFontScaling={true}
	            style={{ fontSize: 20 }}
	        />
          </View>
        );
    }
}

const button_styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },
  button: {
    backgroundColor: 'green'
  }
});


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});