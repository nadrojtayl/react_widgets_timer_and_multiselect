import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Card, ListItem, Button, ButtonGroup } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';





export default class SelectWidget extends React.Component {


    constructor () {
        super();
        this.state = {
            button1_disabled:false,
            button2_disabled:false,
            button3_disabled:false,
            selected_object:{0:false,1:false,2:false}
        }

    }

    update(index){
        this.setState((prevState, props) => {
          var selectedObject = prevState.selected_object
          selectedObject[index] = true;
          return { selected_object: selectedObject}
        });
    }

    onclick(index){
        this.update(index)    
        if(this.allThreeHaveBeenClicked()){
            this.onceallThreeClicked()
        }
    }

    allThreeHaveBeenClicked(){
        if(this.state.selected_object["0"] && this.state.selected_object["1"] && this.state.selected_object["3"]){
            return true
        } else {
            return false
        }
    }

    onceallThreeClicked(){

    }

    render() {
        return (
          <View style={styles.container}>
            <Card title="For the challenge you will need">
            <View style={button_styles.container}>
                <Button style = {button_styles.button} onPress = {() => this.onclick(0)} disabled = {this.state.selected_object["0"]}
                  title='Cards'
                />
                <Button style = {button_styles.button} onPress = {() => this.onclick(1)} disabled = {this.state.selected_object["1"]}
                  title='Coins'
                />
                <Button style = {button_styles.button} onPress = {() => this.onclick(2)} disabled = {this.state.selected_object["2"]}
                  title='Space to move'
                />
            </View>
            </Card>
          </View>
        );
    }
}

const button_styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-evenly'
  },
  button: {
    backgroundColor: 'green'
  }
});


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});